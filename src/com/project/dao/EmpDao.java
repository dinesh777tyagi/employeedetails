package com.project.dao;

import com.project.db.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class EmpDao {


    DBConnection db =  new DBConnection();
    private Connection con = null;

    public void insert(int id , String name, int salary, String city) throws InstantiationException, IllegalAccessException, SQLException, ClassNotFoundException {


        Connection con = db.getConnection();
        String sql="insert into employee values(?,?,?,?);";
        PreparedStatement stmt = con.prepareStatement(sql);

        //Executing insert
        stmt.setInt(1,id);
        stmt.setString(2,name);
        stmt.setInt(3,salary);
        stmt.setString(4,city);
        stmt.execute();
        System.out.println("Employee Detail inserted succesfully");
        stmt.close();
        con.close();
        //db.close(con,stmt);

    }
    public void searchAll() throws IllegalAccessException, SQLException, InstantiationException {
        try {


            Connection con = db.getConnection();
            //taking all data from student
            String sql = "Select * from employee";
            PreparedStatement stmt = con.prepareStatement(sql);

            ResultSet set = stmt.executeQuery(sql);

            //to print the resultset on console
            while (set.next()) {
                int id = set.getInt(1);
                String name = set.getString(2);
                int salary = set.getInt(3);
                String city = set.getString(4);

                System.out.println("Id: " + id);
                System.out.println("Name: " + name);
                System.out.println("Salary: " + salary);
                System.out.println("City: " + city);
                System.out.println("----------------------");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void search() throws IllegalAccessException, SQLException, InstantiationException {
        try{

            Connection con = db.getConnection();

            System.out.println("Enter id to search the Employee Detail:");
            Scanner input = new Scanner(System.in);
            int sid =input.nextInt();

            String sql = "select id,name,salary,city from employee where id=?;";
            PreparedStatement stmt = con.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setInt(1,sid);
            ResultSet rs=stmt.executeQuery();
            if(rs.next()==false)
            {
                System.out.println("No such record found");
            }
            else
            {
                rs.previous();
                while(rs.next())
                {
                    int id= rs.getInt(1);
                    String name= rs.getString(2);
                    int salary=rs.getInt(3);
                    String city= rs.getString(4);

                    System.out.println("Id: "+id);
                    System.out.println("Name: "+name);
                    System.out.println("Salary: "+salary);
                    System.out.println("City: "+city);
                    System.out.println("----------------------");
                }
            }
            stmt.close();
            con.close();
            //db.close(con,stmt);

        }     catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    public void delete() throws SQLException, InstantiationException, IllegalAccessException {
        try
        {

            Connection con = db.getConnection();

            System.out.println("Enter id to delete the Employee Detail:");
            Scanner input = new Scanner(System.in);
            int did =input.nextInt();

            String sql = "delete from employee where id =?;";

            PreparedStatement stmt = con.prepareStatement(sql);

            stmt.setInt(1,did);

            int i = stmt.executeUpdate();
            if (i>0)
            {
                System.out.println("Employee Detail deleted Successfully");
            } else {
                System.out.println("No such record found");

            }
            stmt.close();
            con.close();
            //db.close(con, stmt);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void update() {
        try {

            Connection con = db.getConnection();
            Scanner input = new Scanner(System.in);
            System.out.println("Enter Employee id");
            int id = input.nextInt();
            System.out.println("Update Employee name");
            String name = input.next();
            System.out.println("Update Employee salary");
            int salary = input.nextInt();
            System.out.println("Update Employee city");
            String city = input.next();

            String sql = "update employee set name=?,salary=?,city=? where id= ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1,name);
            stmt.setInt(2,salary);
            stmt.setString(3,city);
            stmt.setInt(4, id);
            int i = stmt.executeUpdate();
            if (i > 0) {
                System.out.println("Employee Detail updated Successfully");
            } else {
                System.out.println("No such record found");

            }
            stmt.close();
            con.close();
            //db.close(con, stmt);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


}
