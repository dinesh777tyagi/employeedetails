package com.project.java;

import com.project.dao.EmpDao;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {


        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Welcome to Employee Details");

            System.out.println("Press 1 Insert Employee Detail");
            System.out.println("Press 2 Display all records Employee");
            System.out.println("Press 3 View by Id ");
            System.out.println("Press 4 Delete a Employee Detail");
            System.out.println("Press 5 Update a Employee Detail");
            System.out.println("Press 6 Exit");
            System.out.println("Enter your choice");

            int c;
            do {
                while (!sc.hasNextInt()) {
                    System.out.println("That's not a number:\nPlease enter choice");
                    sc.next(); // this is important!
                }
                c = sc.nextInt();
            } while (c <= 0);
            //creating object of StudentDaoclass
            EmpDao sd = new EmpDao();
            switch(c)
            {
                case 1:
                    //creating object of student class
                    Employee s = new Employee();
                    // calling getdetails method from student class
                    s.getDetail();
                    //calling insert method from StudentDao class
                    sd.insert(s.id,s.name,s.salary,s.city);

                    break;
                case 2:
                    //calling searchAll method from StudentDao class
                    sd.searchAll();
                    break;

                case 3:
                    //calling search method from StudentDao class
                    sd.search();
                    break;

                case 4:
                    //calling delete method from StudentDao class
                    sd.delete();
                    break;

                case 5:
                    //calling update method from StudentDao class
                    sd.update();
                    break;

                case 6:
                    // exit the console
                    System.exit(0);
                    break;
                default:
                    System.out.println("Enter Valid input\n");
            }
        }
        while(true);
    }
}



